""" Some base classes for reinforcment learning """
import numpy as np
np.random.seed(0)


class QFunction():

    def __call__(self, state, action):
        return self.value(state, action)

    def value(self, state, action):
        raise NotImplementedError()
        
    def grad(self, state, action):
        raise NotImplementedError()
        


class LinearQFunction(QFunction):

    def __init__(self, weights):
        self.weights = weights
    
    def features(self, state, action):
        """ State Action features vector. A vector of same size as weights """
        # See https://numpy.org/doc/stable/reference/generated/numpy.concatenate.html
        return np.concatenate((state, action), axis=None)  # axis=None flattens the state and action vectors

    def value(self, state, action):
        """ The estimated value of this state action pair """
        return np.dot(self.features(state, action), self.weights)

    def grad(self, state, action):
        """ The gradient wrt the weights. """
        return self.features(state, action)



# for later
class ValueFunction():
    def __init__(self, weights):
        self.v_weights = weights
    def get_value(self, state):
        # Could take a features vector of the state
        return np.dot(self.v_weights, state)


# for later
class DeterministicPolicyQFunction():
    def __init__(self, weights, deterministic_policy, value_function):
        assert deterministic_policy.theta.shape == weights.shape
        self.w_weights = weights
        self.V = value_function
        self.μ = deterministic_policy
    def ϕ_sa(self, state, action):
        """ State Action features vector. A vector of same size as theta """
        return np.dot(μ.grad(state), action)
    def get_value(self, state, action):
        μs = self.μ.get_action(state)
        action_diff = action - μs
        ϕsa = self.ϕ_sa(state, action_diff)
        Vs = self.V.get_value(state)
        return np.dot(ϕsa, self.w_weights) + Vs


class RlAgent(object):
    """ An agent interacts with the environment.
    Observes a state, takes an action and receives a reward. """

    def act(self, state):
        """ Observe the given state and react to it. Decide on an action to take and return it."""
        raise NotImplementedError()

    def receive_reward(self, state, reward, terminal):
        """ Receive the reward for the last action taken and boolean indicating whether the last state is terminal.
        The agent can use this to learn and adapt its behavior accordingly."""
        raise NotImplementedError()

    def load_weights(self, filename):
        pass

    def save_weights(self, filename):
        pass

    def total_reward(self):
        """ Returns total reward for the last episode. Used for measuring performance."""
        raise NotImplementedError()


import gym.spaces
import numpy as np

class DiscretePolicySpace(gym.spaces.Discrete):
    """ A finite set of policies """

    def __init__(self, policies):
        """ policies: a list of Policy objects. """
        self.policies = policies
        n = len(policies)
        super(DiscretePolicySpace, self).__init__(n)

    def sample(self):
        """ Returns an index, not a policy."""
        return np.random.randint(self.n)

    def contains(self, x):
        """ Expects x to be an index, not a policy. Checks if x is within range."""
        if isinstance(x, int):
            as_int = x
        elif isinstance(x, (np.generic, np.ndarray)) and (
                x.dtype.kind in np.typecodes['AllInteger'] and x.shape == ()):
            as_int = int(x)
        else:
            return False
        return as_int >= 0 and as_int < self.n

    @property
    def shape(self):
        return ()

    def __repr__(self):
        return "DiscretePolicySpace(%d)" % self.policies

    def __eq__(self, other):
        return self.policies == other.policies


