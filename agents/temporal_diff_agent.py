""" Implementation of n-step semi-gradient sarsa temporal difference learning """

import agents.base_agent
import numpy as np


class TemporalDiffAgent(agents.base_agent.RlAgent):
    """ TD(n) algorithm. (Sutton and Barto p. 200)  """
    def __init__(self, n, policy, qfunction, gamma, alpha):
        super().__init__()
        self.n = n
        # To fix: policy contains the same qfunction. Updating the weights works though.
        self.policy = policy
        self.qfunction = qfunction
        self.gamma = gamma
        self.gamma_pow = np.power(self.gamma, range(self.n + 1))
        self.alpha = alpha
        self.reset()

    def reset(self):
        self.rewards = [None] * self.n
        self.states = [None] * self.n
        self.actions = [None] * self.n
        self.t = 0  # The time step
        # self.end_time = float('inf')  # infinity

    def act(self, state):
        """ Observe the given state and react to it. Decide on an action to take and return it.
            The state and action are stored. This must happen here in the act method instead of the receive_reward method,
            otherwise the initial state is lost.
        """
        action = self.policy.select_action(state)
        k = self.t % self.n
        self.states[k] = state
        self.actions[k] = action
        return action

    def receive_reward(self, state, reward, terminal):
        """ Receive a new state and the reward for the last action taken and boolean indicating whether the new state is terminal.
            Store the reward and perform the n-step temporal difference update. """
        self.t += 1
        k = self.t % self.n
        self.rewards[k] = reward
        update_time = self.t - self.n
        if terminal:
            end_time = self.t
            if update_time < 0:
                update_time = 0
            # continue learning the last rewards for T-n to T-1
            while update_time < end_time:
                self._update_qfunction(None, update_time, end_time)
                update_time += 1
            self.reset()
        elif update_time >= 0:
            self._update_qfunction(state, update_time, None)

    def _update_qfunction(self, state, update_time, end_time=None):
        nr_rewards = self.n if end_time is None else end_time - update_time
        G = sum([self.gamma_pow[i] * self.rewards[(update_time + 1 + i) % self.n] for i in range(nr_rewards)])
        if end_time is None: # or k_upper < end_time:
            action = self.policy.select_action(state)  # TODO, change to greedy selection. This state and action are stored in the 'act' method, not here.
            G += self.gamma_pow[self.n] * self.qfunction(state, action)
        state_ut = self.states[update_time % self.n]
        action_ut = self.actions[update_time % self.n]
        self.qfunction.weights += self.alpha * (G - self.qfunction(state_ut, action_ut)) * self.qfunction.grad(state_ut, action_ut)

    def load_weights(self, filename):
        import pickle
        with open(filename, 'rb') as f:
            self.qfunction.weights = pickle.load(f)

    def save_weights(self, filename):
        import pickle
        with open(filename, 'wb') as f:
            pickle.dump(self.qfunction.weights, f)

    def total_reward(self):
        """ Not implemented because only the last n rewards are stored."""
        raise NotImplementedError()