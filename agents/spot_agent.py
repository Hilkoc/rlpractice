import agents.base_agent
import numpy as np
from types import SimpleNamespace



class SpotEnvAgent(agents.base_agent.RlAgent):
    """ A deterministic agent for benchmarking. """

    def __init__(self, spot_env):
        avg, amp = spot_env.S0, spot_env.VOL * 0.85 * 130  # for exp Ohrstein Uhlenbeck multiply with 130
        theta0 = avg - amp
        theta1 = avg + amp
        self.theta = np.array([theta0, theta1])
        self.policy = SimpleNamespace(theta=self.theta)  # Dummy policy to be compatible with rendering
        self.policy.policy_func = lambda state: {0: np.array([1,0,0]), 1: np.array([0,1,0]), 2: np.array([0,0,1])}[self.act(state)]
        self.reset()

    def reset(self):
        self.rewards = list()

    def act(self, state):
        """ Observe the given state and react to it. Decide on an action to take and return it."""
        if state < self.theta[0]:
            return 0
        if state > self.theta[1]:
            return 2
        return 1  # hold

    def receive_reward(self, state, reward, terminal):
        self.rewards.append(reward)

    def load_weights(self, filename):
        pass

    def save_weights(self, filename):
        pass

    def total_reward(self):
        """ Returns total reward for the last episode. Used for measuring performance."""
        return sum(self.rewards)