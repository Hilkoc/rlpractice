#%matplotlib notebook
import numpy as np

from agents.policy_gradient_agent import PolicyGradientAgent
from envs.spot_env import SpotEnv
from agents.spot_agent import SpotEnvAgent
from session_runner import SessionRunner, plot_performance



def plot_pi(env, agent):
    import pandas as pd
    print(agent.policy.theta)

    s_axis = np.arange(env.S0 - 150 * env.VOL, env.S0 + 150 * env.vol, 0.1)
    pi_func = np.array([agent.policy.policy_func(state) for state in s_axis])

    data = {'h' + str(i) : pi_func[:,i] for i in range(3)}
    # data['total'] = data['h0'] + data['h1'] + data['h2']
    df = pd.DataFrame(data, index=s_axis)
    df.plot()



nr_episodes = 2
max_steps = 5000  # max nr steps per episode

env = SpotEnv(max_steps, start_spot=50, vol=0.1)

agent_pg = PolicyGradientAgent(env.observation_space, env.action_space, alpha=0.07, theta=np.array([49.5, 51.5, 1.6], dtype='float64'))


plot_pi(env, agent_pg)
runner_pg = SessionRunner(env, agent_pg, max_steps=env.MAX_STEPS)
performance_pg = runner_pg.run_session(nr_episodes, nr_peeks=20, render=False)

plot_pi(env, agent_pg)
plot_performance(performance_pg)

print("\nDeterministic agent:")
agent_d = SpotEnvAgent(env)
plot_pi(env, agent_d)
runner_d = SessionRunner(env, agent_d, max_steps=env.MAX_STEPS)
performance_d = runner_d.run_session(nr_episodes=2, nr_peeks=2, render=False)
plot_performance(performance_d)

"""
After training 1000 episodes, with MAX_STEPS=6283 and alpha=0.05 we find 
total reward: 946.498510405275
theta:        [ 91.58443278 107.65802878   2.30397928]

For comparison, the benchmark agent
total reward: 945.4199924697053
theta:       [ 91.22417438 108.77582562]

total reward: 975.8279175020341
theta:       [ 92.68311131 107.31688869]
"""