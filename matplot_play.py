import matplotlib.pyplot as plt
import numpy as np
import math

def g(x):
    return (x - 2) * (x - 5) * (x-8)


x_values = list(range(1,11))
f_x = [-20 + x * x for x in x_values]
g_x = [g(x) for x in x_values]

h_x = [-0.5 * x + 3 for x in x_values]

pairs = dict()
for x, y in enumerate(h_x):
    pairs[x - 5] = y +  0.2 * (x-5)**2

pairs2 = dict()
for x,y in enumerate(h_x):
    pairs2[x+10] = y

# print(pairs2)

def plot_it():
    plt.figure()
    #for i in range(0, len(steps)):
    plt.plot(x_values, f_x, label='f(x)')
    plt.plot(x_values, g_x, label='g(x)')

    xv, yv = map_to_plot(pairs)
    plt.plot(xv, yv, label='h(x)')

    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.legend()
    plt.show()

def map_to_plot(map):
    sorted_keys = sorted(map.keys())
    xv, yv = sorted_keys, [map[k] for k in sorted_keys]
    return xv, yv



def scatter_plot():
    plt.figure()
    plt.scatter([2, 4, 6, 8], [11, 10, 6, 8], label='ordered')
    plt.scatter([2, 8, 6, 4], [10, 7, 5, 9], label='not ordered')

    plt.plot(*map_to_plot(pairs), label='pairs')
    plt.plot(*map_to_plot(pairs2), label='p2')
    x = [2, 8, 6, 4]  # data_dict.keys()
    y = [10,7, 5, 9]  # data_dict.values()
    # plt.scatter(x, y, label='scatter plt')
    plt.legend()
    plt.show()

def plot_planet_values():
    c1, c2 = 0, 0.3
    def g(c, x):
        v = 1 / (x-c)**2
        return v

    def n(c, x):
        v = math.exp(-5 * (x-c)**2)
        return v

    f = g

    x_values = [ -0.95 + i/10.0 for i in  range(0, 30)]
    f1_x = [f(c1,x) for x in x_values]
    f2_x = [f(c2, x) for x in x_values]
    fsum = [ a+b for a,b in zip(f1_x, f2_x)]

    plt.figure()
    #for i in range(0, len(steps)):
    plt.plot(x_values, f1_x, label='f(0,x)')
    plt.plot(x_values, f2_x, label='f(10,x)')
    plt.plot(x_values, fsum, label='som')
    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.legend()
    plt.show()



def oscilloscope():
    # import matplotlib.pyplot as plt
    from matplotlib.lines import Line2D
    import matplotlib.animation as animation


    class Scope:
        def __init__(self, ax, maxt=2, dt=0.02):
            self.ax = ax
            self.dt = dt
            self.maxt = maxt
            self.tdata = [0]
            self.ydata = [0]
            self.line = Line2D(self.tdata, self.ydata)
            self.ax.add_line(self.line)
            self.ax.set_ylim(-.1, 1.1)
            self.ax.set_xlim(0, self.maxt)

        def update(self, y):
            lastt = self.tdata[-1]
            if lastt > self.tdata[0] + self.maxt:  # reset the arrays
                self.tdata = [self.tdata[-1]]
                self.ydata = [self.ydata[-1]]
                self.ax.set_xlim(self.tdata[0], self.tdata[0] + self.maxt)
                self.ax.figure.canvas.draw()

            t = self.tdata[-1] + self.dt
            self.tdata.append(t)
            self.ydata.append(y)
            self.line.set_data(self.tdata, self.ydata)
            return self.line,


    def emitter(p=0.1):
        """Return a random value in [0, 1) with probability p, else 0."""
        while True:
            v = np.random.rand(1)
            if v > p:
                yield 0.
            else:
                yield np.random.rand(1)


    # Fixing random state for reproducibility
    np.random.seed(1984)


    fig, ax = plt.subplots()
    scope = Scope(ax)

    # pass a generator in "emitter" to produce data for the update func
    ani = animation.FuncAnimation(fig, scope.update, emitter, interval=50, blit=True)

    plt.show()



def animate_trading(df=None):
    """ df data frame indexed by time, columns: spot,trade """
    index = np.arange(0,33)
    vol = 15
    start_spot = 50
    time = index / 2
    spot = vol * np.sin(time) + start_spot

    trade = np.zeros(index.shape)
    for i in index:
        if i % 11 == 0 or i % 11 == 3:
            trade[i] = 1 + np.random.uniform()
        elif i % 11 == 7 or i % 11 == 9:
            trade[i] = -1 - np.random.uniform()

    buy = trade>0
    sell = trade<0

    from matplotlib.lines import Line2D
    import matplotlib.pyplot as plt
    import matplotlib.animation as animation

    fig, ax = plt.subplots()

    xdata = [index[0]]
    ydata = [spot[0]]
    line = Line2D(xdata, ydata)

    buy_trades = np.column_stack((index[buy], spot[buy]))
    buy_sizes = 25 * trade[buy]**2
    
    sell_trades = np.column_stack((index[sell], spot[sell]))
    sell_sizes = 25 * trade[sell]**2

    scat_buy = ax.scatter(buy_trades[:1,0], buy_trades[:1,1], s=buy_sizes[:1], c="GREEN")
    scat_sell = ax.scatter(sell_trades[:1,0], sell_trades[:1,1], s=sell_sizes[:1], c="RED")
    # scat_sell = ax.scatter(index[sell], spot[sell], s=25 * trade[sell]**2, c="GREEN")

    def update_f(t):
        # update the graph of spot
        xdata.append(index[t])
        ydata.append(spot[t])
        line.set_data(xdata, ydata)

        # update the trades scatter plot
        bought = buy[:t+1].sum()
        sold = sell[:t+1].sum()
        scat_buy.set_offsets(buy_trades[:bought,:])
        scat_buy.set_sizes(buy_sizes[:bought])
        scat_sell.set_offsets(sell_trades[:sold,:])
        scat_sell.set_sizes(sell_sizes[:sold])
        return line, scat_buy, scat_sell

    # Fixing random state for reproducibility
    np.random.seed(1984)
    
    ax.add_line(line)
    ax.set_ylim(start_spot - vol*1.2, start_spot + vol * 1.2)
    ax.set_xlim(0, len(index))


    # pass a generator in "emitter" to produce data for the update func
    # ani = animation.FuncAnimation(fig, scope.update, emitter, interval=300, blit=True)
    ani = animation.FuncAnimation(fig, update_f, index, interval=1000, blit=True, repeat=False)
    plt.show()




# # plot_planet_values()
# oscilloscope()
# scatter_plot()
animate_trading()