""" Greedy and EpsilonGreedy policy implementations """
import numpy as np


class BasePolicy(object):
    """ The inteface for every policy."""

    def select_action(self, state):
        """ Evaluate the policy on the given state and return an action. """
        raise NotImplementedError

    def grad_log(self, state):
        """ The gradient of the log of the select_action function. For policy gradient algorithms. """
        raise NotImplementedError



class RandomPolicy(BasePolicy):
    def __init__(self, env):
        self.env = env
        
    def select_action(self, state):
        return self.env.action_space.sample()



class GreedyPolicy(BasePolicy):
    """ Greedy or Epsilon Greedy policy for a discrete action space. """
    def __init__(self, action_space, qfunction, epsilon=0):
        super().__init__()
        self.action_space = action_space
        self.qfunction = qfunction
        self.eps = epsilon

    def select_action(self, state):
        if np.random.uniform() < self.eps:
            # print("GreedyPolicy random")
            action = self.action_space.sample()  # np.random.random_integers(0, nb_actions-1)
        else:
            # print("GreedyPolicy determ")
            start = self.action_space.start
            actions = list(range(start, start + self.action_space.n))
            q_values = [self.qfunction.value(state, [action]) for action in actions]
            action_idx = np.argmax(q_values)  # The index of the action
            action = actions[action_idx]
        return action

        