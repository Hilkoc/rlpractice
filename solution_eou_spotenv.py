""" Train an agent to solve the exponential Ornstein-Uhlenbeck environment.
    Following paper by Gordon Ritter, https://ssrn.com/abstract=3015609
"""
import numpy as np

import gym
from agents.base_agent import LinearQFunction
import agents.temporal_diff_agent
from policies.greedy import RandomPolicy, GreedyPolicy
from session_runner import SessionRunner, plot_performance

from envs.spot_env import EouSpotEnv


class DiscretizedPolicy(GreedyPolicy):
    """ Discretize a 1 dimensional action space and apply greedy policy.
        Behavior of this policy is compatible with that for a continuous action space. """
    def __init__(self, action_space, qfunction, epsilon, K):
        super().__init__(action_space, qfunction, epsilon)
        self.action_space = gym.spaces.Discrete(2*K+1, start=int(action_space.low[0]))

    def select_action(self, state):
        """ The action must be wrapped in a list, as if it is a sample from a continuous action space. """
        discrete_action = super().select_action(state)
        continuous_action = [discrete_action]
        return continuous_action


class EouSpotQfunction(LinearQFunction):
    """ Price   space P = ticksize * range(P), P=1000
        Holding space H = [-M, M] = range(2M + 1), M=10
        State   space = PxH
        Action  space = lotsize * [-K, K] = range(2K + 1), K=5

        w_size(P, M , K) = P * (2M + 1) * (2K + 1)
        Therefore, weights have size 1000 * 21 * 11 = 231,000
    """

    @classmethod
    def weight_size(cls, P, M, K):
        return P * (2 * M + 1) * (2 * K + 1)

    def __init__(self, weights, P, M, K):
        assert weights.shape == (self.weight_size(P,M,K),), f"{weights.shape} unequal to {(self.weight_size(P,M,K),)}"
        super().__init__(weights)
        self.ticksize = 0.1
        self.P = P
        self.M = M
        self.K = K

    def features(self, state, action):
        # print(state, action)
        price, holding = state
        p = int(price / self.ticksize)
        h = int(holding) + self.M  # remap [-M,M] to [0, 2M+1]
        a = int(action[0]) + self.K   # remap [-K,K] to [0, 2K+1]
        vec = np.zeros((self.P, 2 * self.M + 1, 2 * self.K + 1), dtype=self.weights.dtype)
        vec[p][h][a] = 1.
        phi = vec.reshape(self.weights.shape)
        # print(f"feature shape: {phi.shape} vec shape {vec.shape}")
        return phi



def make_eouspot_agent(env):
    n = 1
    gamma = 0.999
    alpha = 0.001
    epsilon = 0.1

    w_size = EouSpotQfunction.weight_size(env.P, env.M, env.K)
    # w = np.random.random(w_size)
    w = np.zeros(w_size, dtype=np.float32)
    print(w.shape)
    print("P M K =", (env.P, env.M, env.K))
    qfunction = EouSpotQfunction(w, env.P, env.M, env.K)
    # policy = RandomPolicy(env)
    policy = DiscretizedPolicy(env.action_space, qfunction, epsilon, env.K)
    agent = agents.temporal_diff_agent.TemporalDiffAgent(n, policy, qfunction, gamma, alpha)
    return agent



nr_episodes = 10
max_steps = 1000  # max nr steps per episode
train = True
evaluate = True

P = 1000
M = 10  # 10
K = 5  # 5
env = EouSpotEnv(max_steps, start_spot=40, vol=0.1, P=P, M=M, K=K)


agent = make_eouspot_agent(env)
fname = f"spot_P{P}_M{M}_K{K}.pkl"
agent.load_weights(fname)
runner = SessionRunner(env, agent, max_steps=env.MAX_STEPS)

if train:
    perf = runner.run_session(nr_episodes=nr_episodes, nr_peeks=25, render=False)
    plot_performance(perf)
    agent.save_weights(fname)
if evaluate:
    agent.load_weights(fname)
    agent.policy.eps = 0
    runner.run_session(nr_episodes=1, nr_peeks=1, render=True)

env.close()
