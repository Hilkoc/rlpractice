
class SessionRunner(object):
    """ Runs sessions and episodes. A session is a number of episodes.
    An episode is a number of steps, from reset until done. """

    def __init__(self, env, agent, max_steps=None):
        self.env = env
        self.agent = agent
        self.max_turns_per_episode = max_steps if max_steps is not None else env.MAX_STEPS


    def run_session(self, nr_episodes=1, nr_peeks=1, render=False):
        """
        :param nr_episodes: integer
        :param nr_peeks: positive integer
        :param render: render the first run
        :return: A map showing the reward for each episode during this session
        """
        performance = dict()
        env, agent = self.env, self.agent
        peek = max(1, nr_episodes//nr_peeks)
        for i in range(nr_episodes):

            state = env.reset()
            episode_reward = 0
            t = 0
            done = False
            while not done:
                action = agent.act(state)
                state, reward, done, _ = env.step(action)
                agent.receive_reward(state, reward, done)
                episode_reward += reward
                t += 1
                if t > self.max_turns_per_episode:
                    done = True
            if (1 + i) % peek == 0 or 0 == i:
                print("\nEpisode %i" % i)
                # print(agent.qfunction.weights)
                # print(self.agent.policy.theta)
                print(episode_reward)
                if render:
                    env.render(mode="human")
            performance[i] = episode_reward

        return performance


def plot_performance(performance):
    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(performance.keys(), performance.values(), label='performance')
    plt.xlabel('episode nr')
    plt.ylabel('reward')
    plt.legend()
    plt.show()