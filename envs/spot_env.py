import gym
from gym.spaces import Discrete, Box
import numpy as np
from typing import Optional


class EouSpotEnv(gym.core.Env):
    """ Trade an asset with a fluctuating price 
        following Exponentional Ornstein Uhlenbeck process.
        This environment supports trading partial balance.    
    """
    metadata = {'render.modes': ['human']}


    def __init__(self, max_steps=5000, start_spot=50, vol=0.10, P=1000, M=10, K=5):
        super().__init__()
        self.P = P  # Max price / ticksize
        self.M = M  # Max holiding
        self.K = K  # Max quantity per trade
        self.ticksize = 0.1
        self.dtype = np.float32
        self.action_space = Box(low=-K, high=K, shape=(1,), dtype=self.dtype)  # quantity to sell or buy
        self.observation_space = Box(low=np.array([0,-M], dtype=self.dtype), high=np.array([P * self.ticksize, M], dtype=self.dtype), shape=(2,), dtype=self.dtype)  # price x holding
        self.MAX_STEPS = max_steps
        self.S0 = start_spot  # This is also the equilibrium price
        self.VOL = vol
        self.rng = np.random.default_rng()  #random number generator
        self.mean_rev_rate = 0.13862943611198905  # = np.log(2) / 5
        self.half_kappa = 0.5 * 1e-4
        self.x_eq = np.log(self.S0)
        self.reset()

    def move_spot_mean_rev(self, prev_spot):
        z = self.rng.standard_normal()
        x_prev = np.log(prev_spot/self.S0)
        dx = -self.mean_rev_rate * x_prev + self.VOL * z
        spot = self.S0 * np.exp(x_prev + dx)
        return spot

    def step(self, action):
        assert self.action_space.contains(action)
        # print(f"action {action}")
        action = action[0]
        done = self.time == self.MAX_STEPS
        self.time += 1
        spot = self.move_spot_mean_rev(self.prev_spot)

        # We can sell short, but asset must stay in the range [-M, M]
        #  -self.M - self.asset <= action <= self.M - self.asset:
        # Cash may not go negative: 0 < self.cash
        # Therefore we cannot buy more than we have cash: action * spot <= self.cash

        # For purpose of fees, need to distinguish buying and selling
        if 0 <= action:  # buying
            bought = min(action, self.cash / spot, self.M - self.asset)
            self.asset += bought * (1 - self.fee)
            self.cash -= bought * spot
        else:
            bought = max(action, -self.M - self.asset)
            self.asset += bought
            self.cash -= bought * spot * (1 - self.fee)
        
        balance = self.current_value(spot)
        dv = balance - self.prev_balance
        self.prev_balance = balance
        reward = dv * (1 - self.half_kappa * dv)  # == dv - self.half_kappa * dv**2
        # store the spots and the actions, for rendering.
        self.trades.append(bought)
        self.spots.append(self.prev_spot)  # prev_spot should be stored before updating
        self.prev_spot = spot
        obs = np.array([spot, self.asset], dtype=self.dtype)
        assert self.observation_space.contains(obs), f"{obs} not in {self.observation_space}"
        return obs, reward, done, {}

    def reset(self, seed: Optional[int] = None):
        super().reset(seed)
        self.time = 0
        self.prev_spot = self.S0
        self.asset = 0.0
        self.cash = 100.0
        self.prev_balance = self.current_value(self.S0)
        self.fee = 0.0024  # percentage
        self.spots = []
        self.trades = []
        return np.array([self.S0, self.asset])

    def render(self, mode="human", close=False):
        """ Print to stdout"""
        print(f"balance {self.current_value(self.S0): 16,.2f}   cash {self.cash: 16,.2f}  asset {self.asset: 16,.2f}")
        animate_trading(np.array(self.spots), np.array(self.trades))
        
    def current_value(self, spot):
        " Returns the cash value of the portfolio "
        return self.asset * spot + self.cash



class SpotEnv(gym.core.Env):
    """ Trade an asset with a fluctuating price """
    metadata = {'render.modes': ['human']}

    ACTIONS = ['buy', 'hold', 'sell']

    def __init__(self, max_steps=1000, start_spot=100, vol=10):
        super(SpotEnv, self).__init__()
        self.action_space = Discrete(len(self.ACTIONS))
        self.observation_space = Box(low=0, high=1000, shape=(1,))
        self.MAX_STEPS = max_steps
        self.S0 = start_spot
        self.VOL = vol
        self.rng = np.random.default_rng()  #random number generator
        self.mean_rev_rate = 0.13862943611198905  # = np.log(2) / 5
        self.x_eq = np.log(self.S0)
        self.reset()

    def move_spot_sin(self, prev_spot):
        spot = self.vol * np.sin(self.time / 2.0) + self.start_spot
        return spot

    def move_spot_mean_rev(self, prev_spot):
        z = self.rng.standard_normal()
        x_prev = np.log(prev_spot/self.S0)
        dx = -self.mean_rev_rate * x_prev + self.VOL * z
        spot = self.S0 * np.exp(x_prev + dx)
        return spot


    def step(self, action):
        assert self.action_space.contains(action)
        done = self.time == self.MAX_STEPS
        self.spots.append(self.prev_spot)
        spot = self.move_spot_mean_rev(self.prev_spot)
        self.time += 1
        if self.ACTIONS[action] == 'hold':
            bought = 0
        elif self.ACTIONS[action] == 'buy':
            self.asset += self.cash / spot * (1 - self.fee)
            self.cash = 0
            bought = 1
        elif self.ACTIONS[action] == 'sell':
            self.cash += self.asset * spot * (1 - self.fee)
            self.asset = 0
            bought = -1
        balance = self.current_value(spot)
        stepreturn = balance / self.prev_balance
        self.prev_balance = balance
        obs = self.prev_spot = spot
        reward = 10 * np.log(stepreturn)  # Return log, so rewards are additive
        # store the spots and the actions, for rendering. prev_spot should be stored before updating
        self.trades.append(bought)
        return obs, reward, done, {}

    def reset(self, seed: Optional[int] = None):
        super().reset(seed)
        # print("Reset at time was %i" % self.time)
        self.time = 0
        self.start_spot = self.prev_spot = self.S0
        self.asset = 0.0
        self.cash = 100.0
        self.prev_balance = self.current_value(self.start_spot)
        self.fee = 0.0016  # percentage
        self.vol = self.VOL
        self.spots = []
        self.trades = []
        return self.start_spot

    def render(self, mode="human", close=False):
        """ Print to stdout"""
        print(f"balance {self.current_value(self.S0): 16,.2f}   cash {self.cash: 16,.2f}  asset {self.asset: 16,.2f} ")
        animate_trading(np.array(self.spots), np.array(self.trades))
        
    def current_value(self, spot):
        " Returns the cash value of the portfolio "
        return self.asset * spot + self.cash



def animate_trading(spot, trade):
    """ :param: spot: a numpy array with the spot rates
        :param trade: a numpy array with the amounts bought (or sold for negative values)"""
    assert spot.shape == trade.shape, f"{spot.shape} unequal to {trade.shape}"
    index = np.arange(0, len(spot))
    buy = trade>0
    sell = trade<0

    from matplotlib.lines import Line2D
    import matplotlib.pyplot as plt
    import matplotlib.animation as animation

    fig, ax = plt.subplots()

    xdata = [index[0]]
    ydata = [spot[0]]
    line = Line2D(xdata, ydata)

    buy_trades = np.column_stack((index[buy], spot[buy]))
    buy_sizes = 25 * trade[buy]**2
    scat_buy = ax.scatter(buy_trades[:1,0], buy_trades[:1,1], s=buy_sizes[:1], c="GREEN")

    sell_trades = np.column_stack((index[sell], spot[sell]))
    sell_sizes = 25 * trade[sell]**2
    scat_sell = ax.scatter(sell_trades[:1,0], sell_trades[:1,1], s=sell_sizes[:1], c="RED")


    def update(t):
        # update the graph of spot
        xdata.append(index[t])
        ydata.append(spot[t])
        line.set_data(xdata, ydata)

        # update the trades scatter plot
        bought = buy[:t+1].sum()
        sold = sell[:t+1].sum()
        scat_buy.set_offsets(buy_trades[:bought,:])
        scat_buy.set_sizes(buy_sizes[:bought])
        scat_sell.set_offsets(sell_trades[:sold,:])
        scat_sell.set_sizes(sell_sizes[:sold])
        return line, scat_buy, scat_sell
    
    ax.add_line(line)
    ax.set_ylim(spot.min()*0.99, spot.max()*1.01)
    ax.set_xlim(0, len(index))

    ani = animation.FuncAnimation(fig, update, index, interval=50, blit=True, repeat=False)
    plt.show()


def stash():
    import numpy as np
    import pandas as pd

    seed = None #789123
    rng = np.random.default_rng(seed)

    nr_steps = 2000
    mean_rev_rate = 0.8
    sigma = 15
    x0 = 50

    z = rng.standard_normal(nr_steps)

    x = [x0]
    dx = []

    for i in range(nr_steps):
        s = x[i]
        ds = -mean_rev_rate * (s - x0) + sigma * z[i]
        x.append(s + ds)
        dx.append(ds)
    x.pop()

    df = pd.DataFrame(dict(x=x, dx=dx))
    df.plot()
