""" Train an agent to solve the mountaincar environment """
import gym
from agents.base_agent import LinearQFunction
import agents.temporal_diff_agent
from policies.greedy import RandomPolicy, GreedyPolicy
from session_runner import SessionRunner, plot_performance
import numpy as np

# for tilings
import os
import sys
thisdir = r"/home/hilko/devprojects/rlpractice"
pyfixedreps_dir =  os.path.join(os.path.dirname(thisdir), "PyFixedReps")
sys.path.append(pyfixedreps_dir)
from PyFixedReps import TileCoder



class DiscretizedPolicy(GreedyPolicy):
    """ Discretize a 1 dimensional action space and apply greedy policy.
        Behavior of this policy is compatible with that for a continuous action space. """
    def __init__(self, action_space, qfunction, epsilon):
        super().__init__(action_space, qfunction, epsilon)
        self.action_space = gym.spaces.Discrete(3, start=-1)

    def select_action(self, state):
        """ The action must be wrapped in a list, as if it is a sample from a continuous action space. """
        discrete_action = super().select_action(state)
        continuous_action = [discrete_action]
        return continuous_action



class PoorMountainCarQfunction(LinearQFunction):
    """ Training fails with this Q function. """
    def features(self, state, action):
        # print("MountainCarQfunction(LinearQFunction")
        p, v = state[0], state[1]
        h = np.sin(3*p)
        # print([p, v, v**2, h, action[0]])
        return np.array([p, v, v**2, h, action[0]], dtype=np.float32)
        
        
        
class MountainCarQfunction(LinearQFunction):
    """ Use a tiling to make features """
    def __init__(self, weights):
        super().__init__(weights)
        self.tc = TileCoder({
                # [required]
                'tiles': 9, # how many tiles in each dimension
                'tilings': 1,
                'dims': 2, # shape of the state-vector

                # [optional]
                'actions': 3, # offset the representation by which action was taken (more details below)
                'random_offset': False, # instead of using the default fixed-width offset, randomly generate offsets between tilings
                'input_ranges': [(-1.2, 0.6), (-0.07, 0.07)], # a vector of same length as 'dims' containing (min, max) tuples to rescale inputs
                'scale_output': True, # scales the output by number of active features
        })
        assert weights.shape == (self.tc.features(),), f"Weight vector must have shape ({self.tc.features()},)"

    def features(self, state, action):
        vec = self.tc.encode(state, action[0])
        return vec



def play_once(env, policy, max_steps):
    state = env.reset()
    done = False
    score = 0
    for step in range(max_steps):
        env.render()
        action = policy.select_action(state)
        new_state, reward, done, info = env.step(action)
        
        score += reward
        state = new_state
#         print(action, reward, new_state, done, info)
        if done:
            print(f"Finished with {score}")
            break
    print(f"Total score {score}")
    env.close()
    return score



def make_mountaincar_agent(env):
    n = 16
    gamma = 0.98
    alpha = 0.03
    epsilon = 0.1

    # w_size = env.observation_space.shape[0] + env.action_space.shape[0] + 2
    w_size = 243
    # w = np.random.random(w_size)
    w = np.zeros(w_size)
    # w = np.array([7.78371943,  257.02970655, 1863.40691091,   18.44273245,   15.55067048], dtype=np.float32) # after 50k episodes with n=100
    # w = np.array([6.447083, 8.651462, 9.832361], dtype=np.float32) # score 88.5
    # w = np.array([-27.31492415,  27.47340907,   1.20257703, -11.02460403,   2.60355756]) MC after 500 episodes
    print(w)
    qfunction = MountainCarQfunction(w)
    policy = DiscretizedPolicy(env.action_space, qfunction, epsilon=epsilon)
    agent = agents.temporal_diff_agent.TemporalDiffAgent(n, policy, qfunction, gamma, alpha)
    return agent


env = gym.make('MountainCarContinuous-v0')


# policy = RandomPolicy(env)
# play_once(env, policy, 300)

# Episode 202
# [6.447083 8.651462 9.832361]
# 88.50000000000003


agent = make_mountaincar_agent(env)
fname = "mtcar91_n16.pkl"
agent.load_weights(fname)
runner = SessionRunner(env, agent, max_steps=200)
# runner.run_session(nr_episodes=1, nr_peeks=1, render=True)
if True:
    perf = runner.run_session(nr_episodes=4, nr_peeks=5, render=False)
    plot_performance(perf)
    agent.save_weights(fname)
else:
    agent.load_weights(fname)
agent.policy.eps = 0
runner.run_session(nr_episodes=1, nr_peeks=10, render=True)
env.close()


import matplotlib.pyplot as plt
from matplotlib import cm


def plot_surface(zfunc):
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

    # Rotate the plot
    angle = 240
    ax.view_init(30, angle)
    
    # Make data.
    x = np.arange(-1.21, 0.61, 0.01)
    y = np.arange(-0.071, 0.071, 0.001)
    X, Y = np.meshgrid(x, y)
    #     xv, yv = np.meshgrid(x, y, sparse=False, indexing='xy')
    #         for i in range(nx):
    #             for j in range(ny):
    #                 treat xv[j,i], yv[j,i]
    
    a = np.zeros(X.shape, dtype=np.float32)
    nx = len(x)
    ny = len(y)
    for xi in range(nx):
        for yi in range(ny):
            action = zfunc(X[yi, xi], Y[yi, xi])
            a[yi, xi] = action

    Z = a

    print(X.shape, Y.shape, "Z.shape", Z.shape)
    # Plot the surface.
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)

    # Customize the z axis.
    ax.set_zlim(-1.01, 1.01)
    #ax.zaxis.set_major_locator(LinearLocator(10))
    # A StrMethodFormatter is used automatically
    ax.zaxis.set_major_formatter('{x:.02f}')

    ax.set_xlabel('Position')
    ax.set_ylabel('Velocity')
    ax.set_zlabel('Action')

    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()


def mtcar_action_func(p, v):
    state = np.array([p,v], dtype=np.float32)
    action = agent.act(state)
    return action[0]

plot_surface(mtcar_action_func)